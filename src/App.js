import './App.css';
import pouet from './pouet.png';
function App() {
  return (
    <div className="App">
        <header className="App-header">
            <h1> E-Pouet Pouet</h1>
            <p>The French Tech</p>
            <p>Podcast #12 -> Oussama Amar & Benji Thr</p>
            <img className={'img'} src={pouet} alt=""/>
            <p>Best Pouet Sound on Earth to sell in few weeks</p>
            <p>Available in 2024</p>
            <p>Pouet Pouet Tour 2024</p>
            <iframe width="360" height="315" src="https://www.youtube.com/embed/cTywu9Cv-LU?si=kdZgDZtYp-YWj8Dr"
                    title="YouTube video player" frameBorder="0"
                    allow="accelerometer; autoplay=1; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowFullScreen>
            </iframe>
        </header>
    </div>
  );
}

export default App;
